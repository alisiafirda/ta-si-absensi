@extends('layout.Admin')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-7 align-self-center">
            <h3 class="page-title text-truncate text-dark font-weight-medium mb-1">Selamat Datang {{auth()->user()->name}} !</h3>
            <div class="d-flex align-items-center">
            </div>
        </div>
    </div>
</div>
<br>
        <div class="row">
            <!-- Column -->
            <div class="col-md-6 col-lg-2 col-xlg-3">
                <div class="card card-hover">
                    <div class="box bg-cyan text-center">
                        <h1 class="font-light text-white"><a href="/dashboard"><i class="mdi mdi-view-dashboard"></i></a></h1>
                        <h6 class="text-white">DASHBOARD</h6>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-md-6 col-lg-4 col-xlg-3">
                <div class="card card-hover">
                    <div class="box bg-success text-center">
                        <h1 class="font-light text-white"><a href="/halaman_admin"><i class="mdi mdi-account-check"></i></a></h1>
                        <h6 class="text-white">ADMIN</h6>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-md-6 col-lg-2 col-xlg-3">
                <div class="card card-hover">
                    <div class="box bg-warning text-center">
                        <h1 class="font-light text-white"><a href="/dosen"><i class="mdi mdi-account-multiple"></i></a></h1>
                        <h6 class="text-white">DOSEN</h6>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-md-6 col-lg-2 col-xlg-3">
                <div class="card card-hover">
                    <div class="box bg-danger text-center">
                        <h1 class="font-light text-white"><a href="/jadwal"><i class="mdi mdi-calendar-check"></i></a></h1>
                        <h6 class="text-white">JADWAL</h6>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-md-6 col-lg-2 col-xlg-3">
                <div class="card card-hover">
                    <div class="box bg-info text-center">
                        <h1 class="font-light text-white"><a href="/kelas"><i class="mdi mdi-home-variant"></i></a></h1>
                        <h6 class="text-white">KELAS</h6>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-md-6 col-lg-4 col-xlg-3">
                <div class="card card-hover">
                    <div class="box bg-danger text-center">
                        <h1 class="font-light text-white"><a href="/semester"><i class="mdi mdi-chart-areaspline"></i></a></h1>
                        <h6 class="text-white">SEMESTER</h6>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-md-6 col-lg-2 col-xlg-3">
                <div class="card card-hover">
                    <div class="box bg-info text-center">
                        <h1 class="font-light text-white"><a href="/mahasiswa"><i class="mdi mdi-human-male-female"></i></a></h1>
                        <h6 class="text-white">MAHASISWA</h6>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-md-6 col-lg-2 col-xlg-3">
                <div class="card card-hover">
                    <div class="box bg-cyan text-center">
                        <h1 class="font-light text-white"><a href="/matkul"><i class="mdi mdi-book-open"></i></a></h1>
                        <h6 class="text-white">MATA KULIAH</h6>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-md-6 col-lg-4 col-xlg-3">
                <div class="card card-hover">
                    <div class="box bg-success text-center">
                        <h1 class="font-light text-white"><a href="/pengumuman"><i class="mdi mdi-newspaper"></i></a></h1>
                        <h6 class="text-white">PENGUMUMAN</h6>
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>



<!-- <div class="container-fluid">
    <div class="card-group">
        <div class="card border-right">
            <div class="card-body">
                <div class="d-flex d-lg-flex d-md-block align-items-center">
                    <div>
                        <div class="d-inline-flex align-items-center">
                            <h2 class="text-dark mb-1 font-weight-medium"></h2>
                        </div>
                        <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Data Dosen</h6>
                    </div>
                    <div class="ml-auto mt-md-3 mt-lg-0">
                        <span class="opacity-7 text-muted"><i data-feather="file-plus"></i></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="card border-right">
            <div class="card-body">
                <div class="d-flex d-lg-flex d-md-block align-items-center">
                    <div>
                        <h2 class="text-dark mb-1 font-weight-medium"></h2>
                        <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Data Mahasiswa</h6>
                    </div>
                    <div class="ml-auto mt-md-3 mt-lg-0">
                        <span class="opacity-7 text-muted"><i data-feather="globe"></i></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="card border-right">
            <div class="card-body">
                <div class="d-flex d-lg-flex d-md-block align-items-center">
                    <div>
                        <div class="d-inline-flex align-items-center">
                            <h2 class="text-dark mb-1 font-weight-medium"></h2>
                        </div>
                        <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Data Kelas</h6>
                    </div>
                    <div class="ml-auto mt-md-3 mt-lg-0">
                        <span class="opacity-7 text-muted"><i data-feather="globe"></i></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="d-flex d-lg-flex d-md-block align-items-center">
                    <div>
                        <h2 class="text-dark mb-1 font-weight-medium"></h2>
                        <h6 class="text-muted font-weight-normal mb-0 w-100 text-truncate">Data Mata Kuliah</h6>
                    </div>
                    <div class="ml-auto mt-md-3 mt-lg-0">
                        <span class="opacity-7 text-muted"><i data-feather="globe"></i></span>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="col-lg-12 offset-lg-5">
                        <img src="/admin/assets/images/logokampus.png" style="width:15%;">
                        <div class="mt-2"></div>
                    </div>
                    <div class="col-lg-12 offset-lg-2">
                        <ul class="list-style-none mb-0">
                            <li>
                                <h3 class="text-muted">Sistem Informasi Absensi Mahasiswa Manajemen Informatika</h3>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-8 offset-lg-3">
                        <ul class="list-style-none mb-0 ">
                            <li>
                                <h3 class="text-muted">PSDKU Politeknik Negeri Malang di Kota Kediri</h3>
                            </li>
                        </ul>     
                    </div>               
                </div>
            </div>
        </div>
    </div>


@endsection