@extends('layout.Mahasiswa')
@section('content')
@if (session('status'))

			<div class="alert alert-success">
				{{ session('status') }}
			</div>
			@endif
			<div class="container mx-5">
			<header class="major">
				<h3>ABSENSI MAHASISWA MANAJEMEN INFORMATIKA</h3>
			</header>
			<!-- Form -->
				<section>
					<input type="text" class="form-control" id="jam" name="jam" readonly value="{{\Carbon\Carbon::now()->translatedFormat('d-m-Y H:i:s')}}">
					<br>
					<br>
				</section>


				<form class="row g-3">
				<div class="col-6 col-12-xsmall">
						<label for="inputAddress" class="form-label">NIM</label>
						<input type="text" name="nim" id="ininim" readonly value="{{Auth::user()->username}}">
					</div>
					<div class="col-6 col-12-xsmall">
						<label for="inputAddress" class="form-label">NAMA</label>
						<input type="text" name="nama" id="ininim" readonly value="{{Auth::user()->name}}">
					</div>
					<p>
					<p>
					<div class="col-12">
						<label for="inputAddress" class="form-label">MATA KULIAH</label>
						<input type="text" name="matkul" id="ininim" readonly value="">
					</div>
					<p>
					<div class="col-12">
						<label for="inputAddress2" class="form-label">TANGGAL</label>
						<input type="time" class="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor">
					</div>
					<p>
					<div class="col-12">
						<ul class="actions">
							<li><input type="submit" value="HADIR" class="primary"/></li>
							<li><input type="reset" value="SAKIT"/></li>
							<li><input type="reset" value="IJIN"/></li>
						</ul>
					</div>
				</form>


							
								
            </div>

@endsection