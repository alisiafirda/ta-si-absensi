@extends('layout.Admin')
@section('content')               
                
                <div class="col-md-12">
                        <div class="card">
                        <form action="/update-jadwal/{{$jadwal->id_jadwal}}" method="post">
                            @csrf
                            <div class="card-body">
                                    <h4 class="card-title">Tambah Jadwal</h4>
                                    <div class="form-group row">
                                        <label for="fname"
                                            class="col-sm-3 text-end control-label col-form-label">ID Jadwal</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="jd" id="nidn" class="form-control" autofocus required id="fname"
                                                placeholder="Masukkan ID Jadwal" value="{{$jadwal->id_jadwal}}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-end control-label col-form-label">ID Semester</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="smt" class="form-control" autofocus required id="lname"
                                                placeholder="Masukkan ID Semester" value="{{$jadwal->id_semester}}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lname"
                                            class="col-sm-3 text-end control-label col-form-label">ID Kelas</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="kls" class="form-control" autofocus required id="lname"
                                                placeholder="Masukkan ID Kelas" value="{{$jadwal->id_kelas}}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="email1"
                                            class="col-sm-3 text-end control-label col-form-label">Kode Mata Kuliah</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="kodemk" class="form-control" autofocus required id="email1"
                                                placeholder="Masukkan Kode Mata Kuliah" value="{{$jadwal->kode_matkul}}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="cono1"
                                            class="col-sm-3 text-end control-label col-form-label">Hari</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="hari" class="form-control" autofocus required id="cono1"
                                                placeholder="Masukkan Hari" value="{{$jadwal->hari}}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="cono1"
                                            class="col-sm-3 text-end control-label col-form-label">Tanggal</label>
                                        <div class="col-sm-9">
                                            <input type="date" name="tgl" class="form-control" autofocus required id="cono1"
                                                placeholder="Masukkan Tanggal" value="{{$jadwal->tanggal}}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="cono1"
                                            class="col-sm-3 text-end control-label col-form-label">Jam Mulai</label>
                                        <div class="col-sm-9">
                                            <input type="time" name="jm" class="form-control" autofocus required id="cono1"
                                                placeholder="Masukkan Hari" value="{{$jadwal->jam_mulai}}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="cono1"
                                            class="col-sm-3 text-end control-label col-form-label">Jam Selesai</label>
                                        <div class="col-sm-9">
                                            <input type="time" name="js" class="form-control" autofocus required id="cono1"
                                                placeholder="Masukkan Hari" value="{{$jadwal->jam_selesai}}">
                                        </div>
                                    </div>
                                </div><div class="border-top">
                                <div class="card-body">
                                    <a href="/jadwal" class="btn btn-dark"><i class="fas fa-arrow-alt-circle-left"></i> KEMBALI</a>
                                    <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> SIMPAN</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
@endsection