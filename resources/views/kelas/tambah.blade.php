@extends('layout.Admin')
@section('content')               

                <div class="col-md-12">
                        <div class="card">
                        <form action="/add_kelas" method="post">
                            @csrf
                                <div class="card-body">
                                    <h4 class="card-title">Tambah Kelas</h4>
                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-end control-label col-form-label">Nama Kelas</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="nmkls" class="form-control" autofocus required id="lname"
                                                placeholder="Masukkan Nama Kelas">
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="border-top">
                                    <div class="card-body">
                                        <a href="/kelas" class="btn btn-dark"><i class="fas fa-arrow-alt-circle-left"></i> KEMBALI</a>
                                        <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> SIMPAN</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
@endsection