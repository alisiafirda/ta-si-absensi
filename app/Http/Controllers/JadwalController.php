<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Jadwal;

class JadwalController extends Controller
{
    public function data()
    {
       $jadwal = Jadwal::all();
        
        return view('jadwal.jadwal', compact('jadwal'));
    }

    public function tambah()
    {
        return view('jadwal.tambah');
    }
    public function addProcess(Request $request)
    {
        Jadwal::create([
            'id_jadwal' => $request->jd,
            'id_semester' => $request->smt,
            'id_kelas' => $request->kls,
            'kode_matkul' => $request->kodemk,
            'hari' => $request->hari,
            'tanggal' => $request->tgl,
            'jam_mulai' => $request->jm,
            'jam_selesai' => $request->js
        ]);
        return redirect('/jadwal')->with('status', 'Jadwal berhasil di tambah!');
    }

    public function edit($id_jadwal){
        $jadwal = Jadwal::find($id_jadwal);
        return view('jadwal.edit',compact('jadwal'));
    }

    public function update(Jadwal $jadwal, Request $request){
        Jadwal::where('id_jadwal', $jadwal->id_jadwal)->update([
            'id_jadwal' => $request->jd,
            'id_semester' => $request->smt,
            'id_kelas' => $request->kls,
            'kode_matkul' => $request->kodemk,
            'hari' => $request->hari,
            'tanggal' => $request->tgl,
            'jam_mulai' => $request->jm,
            'jam_selesai' => $request->js,
            'updated_at' =>  date('Y-m-d')
            ]);
            return redirect('jadwal')->with('status', 'Jadwal Berhasil Diubah!'); 
    }

    public function delete($id_jadwal){
        $jadwal = Jadwal::find($id_jadwal);
        $jadwal->delete($jadwal);
        return redirect('jadwal')->with('status', 'Jadwal Berhasil Dihapus!');
    }

    public function datadua()
    {
       $jadwal = Jadwal::all();
        
        return view('jadwal2.index', compact('jadwal'));

    }
}
