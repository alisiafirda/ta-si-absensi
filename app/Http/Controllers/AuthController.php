<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\User;

class AuthController extends Controller
{
    public function index()
    {
        return view('layout.Login');
    }

    public function proses_login(Request $request)
    {
        request()->validate(
            [
                'username' => 'required',
                'password' => 'required',
            ]);
            $kredensil = $request->only('username','password');
        

            if (Auth::attempt($kredensil)) {
                $user = Auth::user();
                if ($user->level == 'admin') {
                    return redirect()->intended('/dashboard');
                }elseif ($user->level == 'mahasiswa') {
                    return redirect()->intended('/awal');
                }
                return redirect()->intended('/')->with('error',"Anda tidak punya akses!");
            }
            auth()->user()->id;
            return redirect('/');
    }
    public function logout(Request $request)
    {
        $request->session()->flush();
        Auth::logout();
        return Redirect('/');
    }
}
