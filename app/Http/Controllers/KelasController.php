<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Kelas;

class KelasController extends Controller
{
    public function data()
    {
       $kelas = Kelas::all();
        
        return view('kelas.kelas', compact('kelas'));
    }
    public function tambah()
    {
        return view('kelas.tambah');
    }

    public function addProcess(Request $request)
    {
        Kelas::create([
            'nama_kelas' => $request->nmkls
        ]);
        return redirect('/kelas')->with('status', 'Kelas berhasil di tambah!');
    }   

    public function edit($id_kelas){
        $kelas = Kelas::find($id_kelas);
        return view('kelas.edit',compact('kelas'));
    }

    public function update($id_kelas, Request $request){
        Kelas::where('id_kelas', $id_kelas)->update([
                'nama_kelas' => $request->nmkls,
                'updated_at' =>  date('Y-m-d')
            ]);
            return redirect('kelas')->with('status', 'Data Kelas Berhasil Diubah!'); 
    }

    public function delete($id_kelas){
        $kelas = Kelas::find($id_kelas);
        $kelas->delete($kelas);
        return redirect('kelas')->with('status', 'Data Kelas Berhasil Dihapus!');
  }



    // public function data()
    // {
    //     $kelas = DB::table('kelas')->get();
    //     $semester= DB::table('semester')->get();
    //     $ganjil = DB::table('kelas')
    //             ->where('id_semester', 1)        
    //             ->get();
    //     $genap = DB::table('kelas')
    //             ->where('id_semester', 2)        
    //             ->get();
    //     return view('kelas.kelas',compact('kelas', 'semester', 'ganjil', 'genap'));

        
    // }


}
