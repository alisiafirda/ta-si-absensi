<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Matkul;

class MatkulController extends Controller
{
    public function data()
    {
        $matkul = Matkul::all();
        return view('matkul.matkul', compact('matkul'));
    }

    public function tambah()
    {
        return view('matkul.tambah');
    }
    public function addProcess(Request $request)
    {
        Matkul::create([
            'kode_matkul' => $request->kodemk,
            'nama_matkul' => $request->namamk,
            'sks' => $request->sks
        ]);
        return redirect('/matkul')->with('status', 'Mata Kuliah berhasil di tambah!');
    }    


    public function edit($kode_matkul){
        $matkul = Matkul::find($kode_matkul);
        return view('matkul.edit',compact('matkul'));
    }

    
    public function update($kode_matkul, Request $request){
        Matkul::where('kode_matkul', $kode_matkul)->update([
            'kode_matkul', $request->kodemk,
            'nama_matkul' => $request->namamk,
            'sks' => $request->sks,
            'updated_at' =>  date('Y-m-d')
            ]);
            return redirect('matkul')->with('status', 'Data Mata Kuliah Berhasil Diubah!'); 
    }

    public function delete($kode_matkul){
        $matkul = Matkul::find($kode_matkul);
        $matkul->delete($matkul);
        return redirect('matkul')->with('status', 'Data Mata Kuliah Berhasil Dihapus!');
  }
  public function datadua()
  {
      $matkul = DB::table('matkul')->get();

      return view('matkul2.index', ['matkul' => $matkul]);
  }

}